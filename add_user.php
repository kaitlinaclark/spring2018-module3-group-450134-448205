<?php
    /*WHAT IS HAPPENING:
        infomation entered from register.html form
        information provided by HTML form:
            -user first name
            -user last name
            -user username
            -user password
        want to hash/salt password
        want to save all entered variables into users table in new_articles database
        when DONE, redirect to login page, so new user can login*/
    include("connect2database.php");//connect to news_article database
    
    //prepare query with ? for parameters
    $add_user = $connect->prepare("insert into users set
                               user_first=?,
                               user_last=?,
                               username=?,
                               password=?");
    if(!$add_user){
      printf("Query Prep Failed: %s \n", $connect->error);
      exit;
    }
    
    //bind parameters to input values
    $add_user->bind_param('ssss', $first_name, $last_name, $userinput, $psw_hash);
        $first_name = $_POST['first'];
        $last_name = $_POST['last'];
        $userinput = $_POST['uname']; //save user input for username
    //create hash password from input password
        $pass = $_POST['psw'];
        $psw_hash = password_hash($pass, PASSWORD_BCRYPT);
    
    //execute query
    $add_user->execute();
    
    //close query
    $add_user->close();
    
    //redirect to login page
    header("Location: http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/login.html");
    
?>
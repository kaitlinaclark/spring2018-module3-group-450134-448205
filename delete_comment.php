<?php
    session_start();
    /*WHAT IS HAPPENING:
		-user wants to delete specified comment
		-comment_id specified in URL*/
	include("connect2database.php");
	
	$delete_query = $connect->prepare("delete from comments
									  where comment_id=?");
	if(!$delete_query){
		printf("Query Prep Failed: %s \n", $connect->error);
		exit;
	}
    //bind parameters
	$delete_query->bind_param('s', $comment_id);
    
	//set values of parameter
		$comment_id = $_GET['comment_id'];
    //execute query
	$delete_query->execute();
	//close query
    $delete_query->close();
    
    //redirect back to userMain page
    $username = $_SESSION['username'];
	header("Location: http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/userMain.php?username=$username");
?>
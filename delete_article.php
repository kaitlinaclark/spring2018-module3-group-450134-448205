<?php
	session_start();
	/*WHAT IS HAPPENING:
		-user wants to delete specified article
		-article_id specified in URL*/
	include("connect2database.php");
	//prepare statement with ? for parameters
	$delete_query = $connect->prepare("delete from articles
									  where article_id=?");
	if(!$delete_query){
		printf("Query Prep Failed: %s \n", $connect->error);
		exit;
	}
	//bind parameters
	$delete_query->bind_param('s', $article_id);
	//set values of parameter
		$article_id = $_GET['article_id'];
	//execute statement
	$delete_query->execute();
	//close statement
	$delete_query->close();
	
	//redirect back to userMain page
	header("Location: http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/userMain.php?username=$username");
?>



<?php
	session_start();
	/*WHAT IS HAPPENING:
		-main page for user:
			displays add article link
			displays logout link
		-shows all articles featured on site
			displays edit/delete link on articles made by user*/
	
	//checks to see if username is set
	if(!isset($_SESSION['username'])){
		header("Location: http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/main.php");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Untitled</title>
</head>

<body>
	<td bgcolor="#00FF00">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 2px">
			<tbody>
				<tr style="background-color: orange;">
					
					<td style="line-height: 12pt; height: 10px;">
						<span class="pagetop">
							<b class="hnname">High School Musical Fan Page</b>
							<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/add_article.html">Add New Article</a>
						</span>
					</td>
					<td style="text-align:right; padding-right:4px;">
						<span class="pagetop">
							<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/logout.php">Logout</a>
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</td>
<?php
	include("connect2database.php");

	//get username and user_id for this session
	$userid = $_SESSION['user_id'];
	$username = $_SESSION['username'];
	
	/*articles table: article_id, author_id (foreign key for user_id in users table)
		get the title and article text from article table
		BUT display articles where author_id = user_id for this session*/
	
	
	echo "<b>Hello, $username !</b>";
	$display_all_articles = $connect->prepare("select id, title, snippet, author_id from articles order by id asc");
	if(!$display_all_articles){
		printf("Query Prep Failed: %s \n", $connect->error);
		exit;
	}
	
	//execute query statement and bind to variables
	$display_all_articles->execute();
	$display_all_articles->bind_result($article_id, $title, $snippet, $author_id);
	
	echo "<ul> \n";
	
	//display articles from query
	while($display_all_articles->fetch()){
		printf("Title: %s \n",
			   htmlentities($title));
		printf("Description: %s \n",
			   htmlentities($snippet));
		
		echo '<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/read_article.php?article_id='.$article_id.'">Read More</a>';
		
		//if articles are by the current user display edit/delete options
		if($author_id == $userid){
			echo ' || <a href=http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/delete_article.php?article_id='.$article_id.'">Delete</a>
				|| <a href=http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/edit_article.php?article_id='.$article_id.'">Edit</a>';
		}
		
		echo "<br></br>";
	}	
	echo "</ul> \n";
?>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--WHAT IS HAPPENING:
	-general main page for non-users
	-display  ALL ARTICLES with a Read More link for each article
		only display Title and snippet description for each article
		article_id passed via URL
	-have link to login at in the header of the page-->
<html>
<head>
<title>Welcome to news channel</title>
    <!--<link rel="stylesheet" type="text/css" href="design/stylesheet.css">-->
</head>
<body>
	<td bgcolor="#00FF00">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 2px">
			<tbody>
				<tr style="background-color: orange;">
					<td style="width:18px; padding-right:4px;">
						<a href="http://www.google.com">Some link</a>
					</td>
					<td style="line-height: 12pt; height: 10px;">
						<span class="pagetop">
							<b class="hnname">High School Musical Fan Page</b>
						</span>
					</td>
					<td style="text-align:right; padding-right:4px;">
						<span class="pagetop">
							<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/login.html">Login</a>
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</td>


<?php
	include("connect2database.php");
	$display_articles_query = $connect->prepare("select id, title, snippet from articles order by id asc");
	if(!$display_articles_query){
		printf("Query Prep Failed: %s \n", $connect->error);
		exit;
	}
	
	$display_articles_query->execute();
	$display_articles_query->bind_result($article_id, $title, $snippet);
	//$display_articles_query->fetch();
	//echo "We've gotten this far with: $id and $title and $snippet as our data";
	echo "<ul> \n";
	while($display_articles_query->fetch()){
		printf("Title: %s \n",
			   htmlentities($title));
		printf("Description: %s \n",
			   htmlentities($snippet));
	
		printf("ID: %s \n",
			   htmlentities($article_id));
		//user options(view, delete) for each article
		echo '<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/read_article.php?article_id='.$article_id.'">Read More</a>';
		
		echo "<br></br>";
	}
	echo "</ul> \n";
?>


</body>
</html>
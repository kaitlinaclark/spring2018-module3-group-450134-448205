<?php
	session_start();
	/*WHAT IS HAPPENING:
		-*/
	include("connect2database.php");
	
	//set up query with ? as parameters
	$display_full_article = $connect->prepare("select
											  title,
											  full_article
											  from articles
										where id = ?");
	if(!$display_full_article){
		printf("Query Prep Failed: %s \n", $connect->error);
		exit;
	}
	//bind parameters
	$display_full_article->bind_param('s', $article_id);
	//set values
		$article_id = (int) $_GET['article_id'];
	//execute statement
	$display_full_article->execute();
	
	//bind query result to values
	$display_full_article->bind_result($title, $text);
	//display full article with title
	while($display_full_article->fetch()){
		echo "<b>";
        printf("Title: %s",
			htmlentities($title));
		echo "</b><br></br>";
		echo "<br></br>";
			printf("Story: %s",
				htmlentities($text));		 
	
	}
	
	echo "<br><a href=\"javascript:history.go(-1)\">Go Back</a></br>";
	
	//prepare statement to display all comments for this article
	$display_comments = $connect->prepare("select
											  comment_id,
											  comments.user_id,
											  comment_text,
											  users.user_first,
											  users.user_last,
											  users.username
											  from comments
											  left join users on (comments.user_id=users.user_id)
										where comments.article_id = ?");
	if(!$display_comments){
		printf("Query Prep Failed: %s \n", $connect->error);
		exit;
	}
	//bind to parameters
	$display_comments->bind_param('s', $article_id);
	
	//execute statement
	$display_comments->execute();
	
	//bind result 
	$display_comments->bind_result($cID, $userid, $cText, $first_name, $last_name, $user);
		//cID = comment_id
		//cText = comment_text
		//first_name
		//last_name
		//username

	//display all comments for this article
	while($display_comments->fetch()){
		//set up info to display first and last name (username) of commenter
		echo "<b>";
        printf("%s %s (%s)",
			htmlentities($first_name),
			htmlentities($last_name),
			htmlentities($user));
		echo "</b><br></br>";
			printf(" %s",
				htmlentities($cText));
        //if currently displayed comment was made by current user
		if($userid == $_SESSION['user_id']){
			echo '<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/edit_comment.php?comment_id='.$cID.'">Edit Comment</a>
			 || <a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/delete_comment.php?comment_id='.$cID.'">Delete Comment</a>';
		}
					 
	
	}
	//option to add comment
	echo '<br>
		<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/add_comment.html?article_id='.$article_id.'">Add Comment</a>
		</br>';
	//close comment statement
	$display_comments->close();
	//close query
	$display_all_articles->close();
		
		
?>



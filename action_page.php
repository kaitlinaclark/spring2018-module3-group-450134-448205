<?php
  //start new session for potential user
 session_start();
 /*WHAT IS HAPPENING:
  This is where login information is entered and checked for validity
       username and password are passed in form login.html
       password is immediately hashed
    check to see if username is in database
    check to see if hashes of passed in psw_hash, match the hash stored in the users table
    if both check out
       save user info to session variables
          username
          user_id
          user_first
          user_last
       user is redirected to userMain page
    if both DO NOT check out
       ERROR message is displayed
       link to register and return t main page is given*/

 include("connect2database.php");//connect to news_article database
    $psw_hash = $_POST['psw'];
    //access all info in users table
    $access_usernames = $connect->prepare("select 
                                          user_id,
                                          password
                                          from users
                                          where username=?");
    if(!$access_usernames){
      printf("Query Prep Failed: %s \n", $connect->error);
      exit;
    }
    //bind parameter
    $access_usernames->bind_param('s', $userinput);
       $userinput = $_POST['uname'];
       
    $access_usernames->execute();
    $access_usernames->bind_result($id, $pass);
    $access_usernames->fetch();
    
    //create hash password from input password
    $userpass = $_POST['psw'];
 
       if(password_verify($userpass, $pass)){
         echo "LOGIN CONFIRMED!!";
         //save variables for this users session
         $_SESSION['username'] = $userinput;
         $_SESSION['user_id'] = (int)$id;
         
         //redirect to main page with user privileges
         header("Location: http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/userMain.php");

       }
       else{
        echo "ERROR: USER NOT FOUND";
        echo '<a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/register.html">Click here to create user account</a>
        || <a href="http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/main.php">Return to Main Page</a>';
        
       }
  
?>

<?php
	session_start();//start session
	/*WHAT IS HAPPENING:
		information from the add_article.html form is passed here
		information will be added to articles table in news_article database
			information entered via form:
				-Article Title
				-Article Description(snippet)
				-Article text
			other info provided in session variable:
				-user_id
		make sure to set user_id of the current user as the author_id of the newly added article*/
	
     include("connect2database.php");

        //prepare query with ? for parameters
        $add_article_query = $connect->prepare("insert into articles set
												 title = ?,
												 snippet = ?,
												 full_article = ?,
												 author_id = ?");
		//bind parameters to input/session values
		$add_article_query->bind_param('ssss', $title, $snippet, $full_article, $userid);
		//info via form
		$title = $_POST['title'];
		$snippet = $_POST['snippet'];
        $full_article= $_POST['full_article'];
		$userid = $_SESSION['user_id'];
		
		//execute query statement
		$add_article_query->execute();
		
        echo "Your article has been successfully submitted.";
		//close query statement
		$add_article_query->close();
		//redirect back to userMain page
		header("Location: http://ec2-18-218-79-182.us-east-2.compute.amazonaws.com/~kaitlinaclark/userMain.php?username=$username");

        
?>

</body>
</html>
